class Dongvat:
    __chan = 0

    def setChan(self, sochan):
        if type(sochan) == int:
            self.__chan = sochan

    def getChan(self):
        return self.__chan


class Dog(Dongvat):
    __ten="Default"
    __chieucao="Default"
    __cannang="Default"

    def __init__(self, ten, chieucao, cannang):
        self.ten = ten
        self.chieucao = chieucao
        self.cannang = cannang

    def __init__(self):
        self.ten = self.__ten
        self.chieucao = self.__chieucao
        self.cannang = self.__cannang

    def setTen(self, ten):
        if len(ten) >= 2:
            self.ten = ten

    def getTen(self):
        return self.ten

    def setChieucao(self, chieucao):
        if chieucao >= 2 and type(chieucao) == int:
            self.chieucao = chieucao

    def getChieucao(self):
        return self.chieucao

    def setCannang(self, cannang):
        if cannang >= 0 and type(cannang) == int:
            self.cannang = cannang

    def getCannang(self):
        return self.cannang


# cho = Dog()
# # cho.setTen("Bitpull")
# # cho.setCannang(39)
# # cho.setChieucao(140)
# # cho.setChan(4)
# print(
#     "Con chó của tôi tên là: {}, và nó nặng: {}, nó cao: {} cm, và nó có {} chân".format(cho.getTen(), cho.getCannang(),
#                                                                                          cho.getChieucao(),
#                                                                                          cho.getChan()))
