import math

# viết 1 vòng lặp lặp từ 1-100
for i in range(1, 101):
    print(i)
i = 1
while (i <= 100):
    print(i)
    i += 1
# vòng lặp từ 0-200 in ra số chẵn xây dựng thành 10 dòng và xắp xếp thành các cột thẳng hàng
for i in range(1, 201):
    if i % 2 == 0:
        print("{:>4}".format(str(i)), end=" ")
        if i % 20 == 0:
            print(" ")


# hàm kiểm tra số nguyên tố nguyên tố
def soNguyenTo(so1):
    if so1 < 2:
        return False
    a=int(math.sqrt(so1)+1)
    for item in range(2,a):
        if so1 % item ==0:
            return False
    return True

print("nhap vao 1 so bat ki")
a=int(input())
if soNguyenTo(a):
    print("Đây là số nguyên tố")
else:
    print("Đây không phải là số nguyên tố")

#chạy từ 1 - 1000 chỉ in ra các số nguyên tố
i=1
while(i<=1000):
    if soNguyenTo(i):
        print(i)
    i+=1