import math

a = min(1, 7, 3, 8, 3, 1, 0, 2, 3)
print(a)
a = max(1, 7, 3, 8, 3, 1, 0, 2, 3)
print(a)
b = -12123123
print(abs(b))
print(math.sqrt(312))
print(math.factorial(9))
print(math.ceil(5.2))
print(math.floor(4.9))
# so sanh
x = 10
y = 10.0
if x == y:
    print("x = y")
if x is y:
    print("X la Y")
else:
    print("x khac y")

x = 100
y = 1234123
x *= y
print(x)
x = 100
y = 1234123
x += y
print(x)
x = 100
y = 1234123
x -= y
print(x)
x = 100
y = 1234123
y /= x
print(x)

x = 10
x **= 3
print(x)

x = 2
x <<= 3
print(x)
x = 10
x >>= 3
print(x)
