d = {
    "name": "Bùi Văn Minh",
    "age": "19",
    "website": "https://facebook.com/bo.cuaban.7169"
}
print(len(d))
d["age"] = "22"
print(d)


# function
def hello():
    print("hello world")


hello()


def tong(x=0, y=0):
    z = x + y
    return z


print(tong(100, 2))


def tong(*data):
    t = 0
    for item in data:
        t += item
    return t


print(tong(10, 352, 5, 56, 4, 4))


def tong(*data):
    kq = []
    for item in data:
        tong = 0
        for n in item:
            tong += n
        kq.append(tong)
    return kq


result = tong([1, 3, 5, 85], [54, 65, 54], [1021, 541, 5465])
print(result)


def dienthoai(**data):
    tong = 0
    for name, price in data.items():
        print("{:10}{:10}".format(name, price))
        tong += price
    return tong


tong = dienthoai(samsung=22000000, iphone=24000000, blackberry=14000000)
print("-" * 20)
print("{:10}{:10}".format("total:", tong))

i = [1, 2, 3, 4, 5, 6, 7, 8]
i = iter(i)
print(type(i))
print(i.__next__())
print(i.__next__())
print(i.__next__())
print(i.__next__())
print(i.__next__())
i = "Bùi Văn minh"
i = iter(i)
print(type(i))
print(i.__next__())
print(i.__next__())
print(i.__next__())
print(i.__next__())
print(i.__next__())
print(i.__next__())
print(i.__next__())
print(i.__next__())
print(i.__next__())
print(i.__next__())
print(i.__next__())
print(i.__next__())


# yield
def myRange(start, stop, step=1):
    while (start < stop):
        yield start
        start += step


for x in myRange(2, 6):
    print(x)

