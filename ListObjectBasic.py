l = [1, 2, 3, 4, 5, 6, 7, 8, 9]
print(type(l), id(l))
for index in range(len(l)):
    print(l[index])
l *= 3
print("-------")
for index in range(len(l)):
    print(l[-index])

k = l.copy()
for index in range(len(k)):
    k[index] = "value-" + "{}".format(index)
    print(k[index])
print(l[0:4])
print(k)
l = [1, 2, 3, 4, 5, 6, 7, 8, 9]
l.append(10)
print(l)
l.insert(10, 11)
print(l)
l.pop(0)
print(l)
print(l.count(10))
l.reverse();
print(l)
l.sort()
print(l)
del l[0:5]
print(l)
a=1212312
print(type(a))
